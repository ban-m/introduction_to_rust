#include <stdio.h>
#include <string.h>

int mean(char* filename){
  FILE *fp;
  fp = fopen(filename,"r");
  if (fp == NULL){
    return 1;
  }
  int sum = 0;
  int num = 0;
  char *line = NULL;
  size_t len = 0;
  while (getline(&line, &len, fp) != -1){
    if (line[0] == '>'){
      // Encounter a new record.
      num += 1;
    }else{
      sum += strlen(line) - 1;
    }
  }
  fclose(fp);
  return sum / num;
}

int main(int argc, char* argv[]){
  if (argc <= 1){
      return 1;
  }
  int result = mean(argv[1]);
  printf("%d\n",result);
  return 0;
}
