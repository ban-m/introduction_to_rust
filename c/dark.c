#include <stdio.h>

void void_conversion(void* x){
  *(int *)x = 1;
}


int main(){
  int x = 0;
  void_conversion((void *)x);
  printf("%d\n",x);
}
