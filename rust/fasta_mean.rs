use std::io::Read;
use std::path::Path;
use std::fs::File;

fn mean(file:&str)->std::io::Result<usize>{
    let mut file = File::open(Path::new(file))?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;
    let mut sum = 0;
    let mut num = 0;
    for line in input.lines(){
        if line.starts_with('>'){
            num += 1;
        }else{
            sum += line.len();
        }
    }
    Ok(sum / num)
}

fn main()->std::io::Result<()>{
    let args:Vec<_> = std::env::args().collect::<Vec<String>>();
    let result = mean(&args[1])?;
    println!("{}",result);
    Ok(())
}
