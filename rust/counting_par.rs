use std::fs::File;
use std::io::{BufRead,BufReader};
use std::path::Path;
// use std::sync::mpsc::channel;
use std::thread;
const THR: usize = 2;

#[inline]
fn count_line(line: String) -> [u32; 4] {
    let mut sum = [0; 4];
    for char in line.chars() {
        match char {
            'A' => sum[0] += 1,
            'C' => sum[1] += 1,
            'G' => sum[2] += 1,
            'T' => sum[3] += 1,
            _ => {}
        }
    }
    sum
}

#[inline]
fn count_lines(lines:Vec<String>)->[u32;4]{
    let mut sum = [0;4];
    for line in lines{
        let num = count_line(line);
        for i in 0..4 {
            sum[i] += num[i]
        }
    }
    sum
}


fn main() -> std::io::Result<()> {
    let args: Vec<_> = std::env::args().collect::<Vec<String>>();
    let mut sum = [0; 4];
    let input:Vec<String> = BufReader::new(File::open(Path::new(&args[1]))?).lines()
        .filter_map(|e|e.ok()).collect();
    let bucket_size = input.len() / THR;    
    let handlers: Vec<_> = (0..THR)
        .map(|i|{
            let bucket = if i == THR - 1{
                input[i*bucket_size..].to_vec()
            }else{
                input[i*bucket_size..(i+1)*bucket_size].to_vec()
            };
            thread::spawn(move || count_lines(bucket))
        })
        .collect();
    for handler in handlers {
        let num = handler.join().unwrap();
        for i in 0..4 {
            sum[i] += num[i]
        }
    }
    println!("[A,C,G,T] = {:?}", sum);
    Ok(())
}
