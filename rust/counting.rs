use std::io::Read;
use std::path::Path;
use std::fs::File;

#[inline]
fn count_line(line:&str)->[u32;4]{
    let mut sum = [0;4];
    for char in line.chars(){
        match char {
            'A' => sum[0] += 1,
            'C' => sum[1] += 1,
            'G' => sum[2] += 1,
            'T' => sum[3] += 1,
            _ => {},
        }
    }
    sum
}

fn count(file:&str)->std::io::Result<[u32;4]>{
    let mut file = File::open(Path::new(file))?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;
    let mut sum = [0;4];
    for line in input.lines(){
        if !line.starts_with('>'){
            let num = count_line(line);
            for i in 0..4{
                sum[i] += num[i]
            }
        }
    }
    Ok(sum)
}

fn main()->std::io::Result<()>{
    let args:Vec<_> = std::env::args().collect::<Vec<String>>();
    let result = count(&args[1])?;
    println!("[A,C,G,T] = {:?}",result);
    Ok(())
}
