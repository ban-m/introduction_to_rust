import sys

def mean(file):
    with open(file,'r') as file:
        sum = 0;
        num = 0;
        for line in file:
            if line.startswith('>'):
                num += 1
            else:
                sum += len(line) - 1
        return int(sum / num)


if __name__ == '__main__':
    ARGV = sys.argv
    result = mean(ARGV[1])
    print("{}".format(result));
