import sys

def count_line(line):
    sum = [0,0,0,0]
    for char in line:
        if char == 'A':
            sum[0] +=1
        elif char  == 'C':
            sum[1] += 1
        elif char == 'G':
            sum[2] += 1
        elif char == 'T':
            sum[3] += 1
    return sum


def count(file):
    with open(file,'r') as file:
        sum = [0,0,0,0];
        for line in file:
            if not line.startswith('>'):
                num = count_line(line);
                for i in range(4):
                    sum[i] += num[i];
        return sum


if __name__ == '__main__':
    ARGV = sys.argv
    result = count(ARGV[1])
    print("[A,C,G,T] = {}".format(result));
